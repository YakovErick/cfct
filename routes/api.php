<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group( [ 'middleware' => 'auth:api', 'namespace' => 'Apis' ], function () {


    Route::get('/children', 'ChildrenController@index');
    Route::get('/children/{id}', 'ChildrenController@show');
    Route::post('/children', 'ChildrenController@store');
    Route::patch('/children/{id}', 'ChildrenController@update');
    Route::put('/children/{id}', 'ChildrenController@update');
    Route::delete('/children/{id}', 'ChildrenController@destroy');


    Route::get('/sponsors', 'SponsorsController@index');
    Route::get('/sponsors/all', 'SponsorsController@all');
    Route::get('/sponsors/{id}', 'SponsorsController@show');
    Route::post('/sponsors', 'SponsorsController@store');
    Route::patch('/sponsors/{id}', 'SponsorsController@update');
    Route::put('/sponsors/{id}', 'SponsorsController@update');
    Route::delete('/sponsors/{id}', 'SponsorsController@destroy');

    Route::get('/sponsorships', 'SponsorshipController@index');
    Route::get('/sponsorships/{id}', 'SponsorshipController@show');
    Route::post('/sponsorships', 'SponsorshipController@store');
});