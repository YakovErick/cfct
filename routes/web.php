<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/children', 'ChildrenController@index')->name('children');
    Route::get('/children/new', 'ChildrenController@create')->name('children');
    Route::get('/children/{id}', 'ChildrenController@show')->name('children');
    Route::get('/children/{id}/edit', 'ChildrenController@edit')->name('children');
    Route::post('/children/{id}/edit-post', [
        'as' => 'edit_child',
        'uses' => 'ChildrenController@update'
    ]);

    Route::get('/sponsors', 'SponsorsController@index')->name('sponsor');
    Route::get('/sponsors/new', 'SponsorsController@create')->name('sponsor');
    Route::get('/sponsors/{id}', 'SponsorsController@show')->name('sponsor');
    Route::get('/sponsors/{id}/edit', 'SponsorsController@edit')->name('sponsor');
    Route::post('/sponsors/{id}/edit-post', [
        'as' => 'edit_sponsor',
        'uses' => 'SponsorsController@update'
    ]);

    Route::get('/sponsorship/{child_id}/create', 'SponsorshipController@create');
    Route::get('/sponsorship/{child_id}/contribute', 'SponsorshipController@contribute');

    Route::post('/sponsorship/{child_id}/contribute/post', 'SponsorshipController@postContribution');
});
