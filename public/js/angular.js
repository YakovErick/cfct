/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/angular/controllers/AuthController.js":
/*!*********************************************************!*\
  !*** ./resources/angular/controllers/AuthController.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var authApp = angular.module("authApp", ['appService']);
authApp.config(function ($httpProvider) {
  var token = document.head.querySelector('meta[name="csrf-token"]');
  $httpProvider.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
  $httpProvider.defaults.headers.common['Authorization'] = 'Bearer ' + token.content;
  $httpProvider.defaults.headers.post = {
    "X-Requested-With": "XMLHttpRequest",
    "__RequestVerificationToken": $('[name=__RequestVerificationToken]').val()
  };
});

/***/ }),

/***/ "./resources/angular/controllers/ChildrenController.js":
/*!*************************************************************!*\
  !*** ./resources/angular/controllers/ChildrenController.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var childApp = angular.module("childApp", ['ChildrenService']);
childApp.controller('ChildrenController', function ($scope, ChildData) {
  $scope.relationship_person = {
    relationship_id: 1
  };
  $scope.relationship_persons = [];
  $scope.child = {};
  $scope.children = [];
  $scope.guardian_added = false;
  fetchChildren();

  function fetchChildren() {
    ChildData.fetchChildren().then(function (_ref) {
      var data = _ref.data;
      $scope.children = data.data;
    }, function () {
      alert('Failed to fetch children!');
    });
  }

  $scope.addGuardian = function () {
    $scope.relationship_persons.push($scope.relationship_person);
    $scope.guardian_added = true;
    $scope.relationship_person = {};
  };

  $scope.submitChild = function () {
    ChildData.addChild({
      child: $scope.child,
      relationship_persons: $scope.relationship_persons
    }).then(function (_ref2) {
      var data = _ref2.data;

      if (data.status == 406) {
        alert("Kindly fill the missing fields and proceed");
      } else {
        location.replace('/children');
        alert("Created successfully");
      }
    }, function () {
      alert('Failed to submit!');
    });
  };

  $scope.viewChild = function (id) {
    location.replace('/children/' + id);
  };

  $scope.editChild = function (id) {
    location.replace('/children/' + id + '/edit');
  };

  $scope.deleteChild = function (id) {
    var confi = confirm("Are you sure to delete a child!");

    if (confi === true) {
      ChildData.deleteChild(id).then(function (_ref3) {
        var data = _ref3.data;
        fetchChildren();

        if (data.status == 406) {
          alert(data.message);
        }
      }, function () {
        fetchChildren();
      });
    }
  };

  $scope.assignSponsor = function (id) {
    location.replace('/sponsorship/' + id + '/create');
  };
});

/***/ }),

/***/ "./resources/angular/controllers/SponsorsController.js":
/*!*************************************************************!*\
  !*** ./resources/angular/controllers/SponsorsController.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var sponsorApp = angular.module("sponsorApp", ['SponsorsService']);
sponsorApp.controller('SponsorsController', function ($scope, SponsorData) {
  $scope.sponsor = {};
  $scope.sponsors = [];
  fetchSponsors();

  function fetchSponsors() {
    SponsorData.fetchSponsors().then(function (_ref) {
      var data = _ref.data;
      $scope.sponsors = data.data;
    }, function () {
      alert('Failed to fetch sponsor!');
    });
  }

  $scope.submitSponsor = function () {
    SponsorData.addSponsor($scope.sponsor).then(function (_ref2) {
      var data = _ref2.data;

      if (data.status == 406) {
        alert("Kindly fill the missing fields and proceed");
      } else {
        location.replace('/sponsors');
        alert("Created successfully");
      }
    }, function () {
      alert('Failed to submit!');
    });
  };

  $scope.viewSponsor = function (id) {
    location.replace('/sponsors/' + id);
  };

  $scope.editSponsor = function (id) {
    location.replace('/sponsors/' + id + '/edit');
  };

  $scope.deleteSponsor = function (id) {
    var confi = confirm("Are you sure to delete a sponsor!");

    if (confi === true) {
      SponsorData.deleteSponsor(id).then(function (_ref3) {
        var data = _ref3.data;
        fetchSponsors();

        if (data.status == 406) {
          alert(data.message);
        }
      }, function () {
        fetchSponsors();
      });
    }
  };
});

/***/ }),

/***/ "./resources/angular/controllers/SponsorshipController.js":
/*!****************************************************************!*\
  !*** ./resources/angular/controllers/SponsorshipController.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var sponsorshipApp = angular.module("sponsorshipApp", ['SponsorshipService']);
sponsorshipApp.controller('SponsorshipController', function ($scope, SponsorshipData) {
  $scope.child = {};
  $scope.sponsorship = {};
  $scope.sponsors = [];
  fetchSponsors();

  function fetchSponsors() {
    SponsorshipData.fetchSponsors().then(function (_ref) {
      var data = _ref.data;
      $scope.sponsors = data;
    }, function () {
      alert('Failed to fetch sponsor!');
    });
  }

  $scope.fetchChild = function (id) {
    $scope.sponsorship.child_id = id;
    SponsorshipData.getChild(id).then(function (_ref2) {
      var data = _ref2.data;
      $scope.child = data.child;
    }, function () {
      alert('Failed to fetch child');
    });
  };

  $scope.assignSponsor = function () {
    SponsorshipData.addSponsorship($scope.sponsorship).then(function () {
      location.replace('/children/' + $scope.sponsorship.child_id);
    }, function () {
      alert('Failed to create sponsorship!');
    });
  };
});

/***/ }),

/***/ "./resources/angular/services/App.js":
/*!*******************************************!*\
  !*** ./resources/angular/services/App.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var appService = angular.module('appService', []).factory('AuthData', ['$http', function ($http) {
  return AuthData;
}]);

/***/ }),

/***/ "./resources/angular/services/Children.js":
/*!************************************************!*\
  !*** ./resources/angular/services/Children.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var ChildrenService = angular.module('ChildrenService', []);
ChildrenService.config(function ($httpProvider) {
  var token = document.head.querySelector('meta[name="csrf-token"]');
  $httpProvider.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
  $httpProvider.defaults.headers.common['Authorization'] = 'Bearer ' + token.content;
  $httpProvider.defaults.headers.get = {
    "X-Requested-With": "XMLHttpRequest",
    "__RequestVerificationToken": $('[name=__RequestVerificationToken]').val()
  };
});
ChildrenService.factory('ChildData', ['$http', function ($http) {
  var ChildData = {};

  ChildData.fetchChildren = function () {
    return $http.get('/api/children');
  };

  ChildData.addChild = function (child) {
    return $http.post('/api/children', child);
  };

  ChildData.deleteChild = function (id) {
    return $http["delete"]('/api/children/' + id);
  };

  return ChildData;
}]);

/***/ }),

/***/ "./resources/angular/services/Sponsors.js":
/*!************************************************!*\
  !*** ./resources/angular/services/Sponsors.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var SponsorsService = angular.module('SponsorsService', []);
SponsorsService.config(function ($httpProvider) {
  var token = document.head.querySelector('meta[name="csrf-token"]');
  $httpProvider.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
  $httpProvider.defaults.headers.common['Authorization'] = 'Bearer ' + token.content;
  $httpProvider.defaults.headers.get = {
    "X-Requested-With": "XMLHttpRequest",
    "__RequestVerificationToken": $('[name=__RequestVerificationToken]').val()
  };
});
SponsorsService.factory('SponsorData', ['$http', function ($http) {
  var SponsorData = {};

  SponsorData.fetchSponsors = function () {
    return $http.get('/api/sponsors');
  };

  SponsorData.addSponsor = function (sponsor) {
    return $http.post('/api/sponsors', sponsor);
  };

  SponsorData.deleteSponsor = function (id) {
    return $http["delete"]('/api/sponsors/' + id);
  };

  return SponsorData;
}]);

/***/ }),

/***/ "./resources/angular/services/Sponsorship.js":
/*!***************************************************!*\
  !*** ./resources/angular/services/Sponsorship.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var SponsorshipService = angular.module('SponsorshipService', []);
SponsorshipService.config(function ($httpProvider) {
  var token = document.head.querySelector('meta[name="csrf-token"]');
  $httpProvider.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
  $httpProvider.defaults.headers.common['Authorization'] = 'Bearer ' + token.content;
  $httpProvider.defaults.headers.get = {
    "X-Requested-With": "XMLHttpRequest",
    "__RequestVerificationToken": $('[name=__RequestVerificationToken]').val()
  };
});
SponsorshipService.factory('SponsorshipData', ['$http', function ($http) {
  var SponsorshipData = {};

  SponsorshipData.fetchSponsorships = function () {
    return $http.get('/api/sponsorships');
  };

  SponsorshipData.addSponsorship = function (sponsorship) {
    return $http.post('/api/sponsorships', sponsorship);
  };

  SponsorshipData.deleteSponsorships = function (id) {
    return $http["delete"]('/api/sponsors/' + id);
  };

  SponsorshipData.getChild = function (id) {
    return $http.get('/api/children/' + id);
  };

  SponsorshipData.fetchSponsors = function () {
    return $http.get('/api/sponsors/all');
  };

  return SponsorshipData;
}]);

/***/ }),

/***/ "./resources/sass/app.scss":
/*!*********************************!*\
  !*** ./resources/sass/app.scss ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** multi ./resources/angular/controllers/AuthController.js ./resources/angular/controllers/ChildrenController.js ./resources/angular/controllers/SponsorsController.js ./resources/angular/controllers/SponsorshipController.js ./resources/angular/services/App.js ./resources/angular/services/Children.js ./resources/angular/services/Sponsors.js ./resources/angular/services/Sponsorship.js ./resources/sass/app.scss ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /home/yakov/work/practice/cfct/resources/angular/controllers/AuthController.js */"./resources/angular/controllers/AuthController.js");
__webpack_require__(/*! /home/yakov/work/practice/cfct/resources/angular/controllers/ChildrenController.js */"./resources/angular/controllers/ChildrenController.js");
__webpack_require__(/*! /home/yakov/work/practice/cfct/resources/angular/controllers/SponsorsController.js */"./resources/angular/controllers/SponsorsController.js");
__webpack_require__(/*! /home/yakov/work/practice/cfct/resources/angular/controllers/SponsorshipController.js */"./resources/angular/controllers/SponsorshipController.js");
__webpack_require__(/*! /home/yakov/work/practice/cfct/resources/angular/services/App.js */"./resources/angular/services/App.js");
__webpack_require__(/*! /home/yakov/work/practice/cfct/resources/angular/services/Children.js */"./resources/angular/services/Children.js");
__webpack_require__(/*! /home/yakov/work/practice/cfct/resources/angular/services/Sponsors.js */"./resources/angular/services/Sponsors.js");
__webpack_require__(/*! /home/yakov/work/practice/cfct/resources/angular/services/Sponsorship.js */"./resources/angular/services/Sponsorship.js");
module.exports = __webpack_require__(/*! /home/yakov/work/practice/cfct/resources/sass/app.scss */"./resources/sass/app.scss");


/***/ })

/******/ });