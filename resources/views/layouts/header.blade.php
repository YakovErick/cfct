<div class="container">

    <a class="navbar-brand" href="{{ url('/') }}">
        <b>CFCT</b>
    </a>

    @guest

    @else

        <div class="main-menu main-menu-items">
            <a class="navbar-brand" href="{{ url('/home') }}">
                home
            </a>

            <a class="navbar-brand" href="{{ url('/children') }}">
                children
            </a>

            <a class="navbar-brand" href="{{ url('/sponsors') }}">
                sponsors
            </a>
        </div>

    @endguest


    <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <ul class="navbar-nav ml-auto">

            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>

                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else

                <li>
                    <a class="nav-link" href="#">{{ Auth::user()->name }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            @endguest
        </ul>
    </div>
</div>