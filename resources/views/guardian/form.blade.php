<div class="row">
    <div class="col">
        <label for="" class="form-label required">Enter guardian first name</label>
        <input type="text" class="form-control" ng-model="relationship_person.first_name" placeholder="first name">
    </div>

    <div class="col">
        <label for="" class="form-label">Enter guardian middle name</label>
        <input type="text" class="form-control" ng-model="relationship_person.middle_name" placeholder="middle name">
    </div>

    <div class="col">
        <label for="" class="form-label required">Enter guardian last name</label>
        <input type="text" class="form-control" ng-model="relationship_person.last_name" placeholder="last name">
    </div>
</div>

<br>

<div class="row">
    <div class="col">
        <label for="" class="form-label required">Enter guardian email</label>
        <input type="text" class="form-control" ng-model="relationship_person.email" placeholder="email address">
    </div>

    <div class="col">
        <label for="" class="form-label required">Enter guardian phone</label>
        <input type="text" class="form-control" ng-model="relationship_person.phone" placeholder="phone">
    </div>

    <div class="col">
        <label for="" class="form-label">Enter guardian address</label>
        <input type="text" class="form-control" ng-model="relationship_person.address" placeholder="address">
    </div>
</div>

<br>

<div class="row">
    <div class="col">
        <label for="" class="form-label required">Choose relationship type</label>

        <select name="relationship_id" ng-model="relationship_person.relationship_id"  class="form-control">

            @foreach($relationships as $relationship)
                <option value="{{ $relationship->id }}">{{ $relationship->name }}</option>
            @endforeach

        </select>
    </div>

    <div class="col">
    </div>

    <div class="col">
    </div>
</div>

<br>

<div class="row">
    <div class="col">
        <label for="">You can add as many gurdians as possible, just fill the form and click add guardian below.</label>
        <br>
        <button type="button" class="btn btn-primary btn-sm" ng-click="addGuardian()">ADD GUARDIAN </button>
    </div>
</div>