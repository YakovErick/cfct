@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header custom-card">
                        <div class="custom-card__left">
                            <b>SPONSORS MANAGEMENT</b>
                        </div>

                        <div class="custom-card__right">
                            <a href="/sponsors/new" class="btn btn-success">NEW SPONSOR</a>
                        </div>
                    </div>

                    <div class="card-body" ng-app="sponsorApp" ng-controller="SponsorsController">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>NAME</th>
                                <th>EMAIL ADDRESS</th>
                                <th>PHONE</th>
                                <th>ADDRESS</th>
                                <th>ACTION</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr ng-repeat="sponsor in sponsors">
                                <td>@{{ sponsor.name }}</td>
                                <td>@{{ sponsor.email }}</td>
                                <td>@{{ sponsor.phone }}</td>
                                <td>@{{ sponsor.address }}</td>
                                <td>
                                    <button ng-click="viewSponsor(sponsor.id)" class="btn btn-info btn-sm">VIEW</button>
                                    <button ng-click="editSponsor(sponsor.id)" class="btn btn-primary btn-sm">EDIT</button>
                                    <button ng-click="deleteSponsor(sponsor.id)" class="btn btn-danger btn-sm">DELETE</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
