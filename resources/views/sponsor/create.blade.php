@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header custom-card">
                        <div class="custom-card__left">
                            <b>NEW SPONSOR FORM</b>
                        </div>

                        <div class="custom-card__right">
                            <a href="/sponsors" class="btn btn-outline-success">BACK</a>
                        </div>
                    </div>

                    <div class="card-body" ng-app="sponsorApp" ng-controller="SponsorsController">

                        <br>

                        <form>
                            <div class="row">
                                <div class="col">
                                    <label for="" class="form-label required">Enter sponsor first name</label>
                                    <input type="text" class="form-control" ng-model="sponsor.first_name" placeholder="first name">
                                </div>

                                <div class="col">
                                    <label for="" class="form-label">Enter sponsor middle name</label>
                                    <input type="text" class="form-control" ng-model="sponsor.middle_name" placeholder="middle name">
                                </div>

                                <div class="col">
                                    <label for="" class="form-label required">Enter sponsor last name</label>
                                    <input type="text" class="form-control" ng-model="sponsor.last_name" placeholder="last name">
                                </div>
                            </div>

                            <br>

                            <div class="row">
                                <div class="col">
                                    <label for="" class="form-label required">Enter sponsor email</label>
                                    <input type="text" class="form-control" ng-model="sponsor.email" placeholder="email address">
                                </div>

                                <div class="col">
                                    <label for="" class="form-label required">Enter sponsor phone</label>
                                    <input type="text" class="form-control" ng-model="sponsor.phone" placeholder="phone">
                                </div>

                                <div class="col">
                                    <label for="" class="form-label">Enter sponsor address</label>
                                    <input type="text" class="form-control" ng-model="sponsor.address" placeholder="address">
                                </div>
                            </div>

                            <br>

                            <div class="row">
                                <div class="col">
                                    <button type="button" class="btn btn-success" ng-click="submitSponsor()">SUBMIT</button>
                                </div>
                            </div>
                        </form>

                        <br>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
