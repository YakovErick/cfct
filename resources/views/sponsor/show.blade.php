@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header custom-card">
                        <div class="custom-card__left">
                            <b>SPONSOR INFORMATION</b>
                        </div>

                        <div class="custom-card__right">
                            <a href="/sponsors" class="btn btn-outline-success">BACK</a>
                        </div>
                    </div>

                    <div class="card-body">

                        <div class="custom-card">
                            <div class="custom-card__left">
                                <b></b>
                            </div>

                            <div class="custom-card__right">

                                <a href="/sponsors/{{ $sponsor['id'] }}/edit" class="btn btn-success btn-sm">EDIT</a>

                            </div>
                        </div>

                        <br>

                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <td>{{ $sponsor['name'] }}</td>
                            </tr>

                            <tr>
                                <th>Phone</th>
                                <td>{{ $sponsor['phone'] }}</td>
                            </tr>

                            <tr>
                                <th>Email</th>
                                <td>{{ $sponsor['email'] }} Years</td>
                            </tr>

                            <tr>
                                <th>Address</th>
                                <td>{{ $sponsor['address'] }} </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="delete_sponsor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">DELETE SPONSOR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    Are you sure you want to delete the sponsor
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <a href="/sponsors/{{ $sponsor['id'] }}/delete" type="button" class="btn btn-primary">Yes</a>
                </div>
            </div>
        </div>
    </div>
@endsection
