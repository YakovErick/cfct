@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header custom-card">
                        <div class="custom-card__left">
                            <b>EDIT SPONSOR</b>
                        </div>

                        <div class="custom-card__right">
                            <a href="/sponsors" class="btn btn-success">BACK</a>
                        </div>
                    </div>

                    <div class="card-body">

                        <br>

                        <form method="POST" action="{{ route('edit_sponsor', [ 'id' => $sponsor->id ]) }}">

                            @csrf

                            <div class="row">
                                <div class="col">
                                    <label for="" class="form-label required">Enter sponsor first name</label>
                                    <input type="text" class="form-control" value="{{ $sponsor->first_name }}" name="first_name" placeholder="first name">
                                </div>

                                <div class="col">
                                    <label for="" class="form-label">Enter sponsor middle name</label>
                                    <input type="text" class="form-control" value="{{ $sponsor->middle_name }}" name="middle_name" placeholder="middle name">
                                </div>

                                <div class="col">
                                    <label for="" class="form-label required">Enter sponsor last name</label>
                                    <input type="text" class="form-control" value="{{ $sponsor->last_name }}" name="last_name" placeholder="last name">
                                </div>
                            </div>

                            <br>

                            <div class="row">
                                <div class="col">
                                    <label for="" class="form-label required">Enter sponsor email</label>
                                    <input type="text" class="form-control" value="{{ $sponsor->email }}" name="email" placeholder="email address">
                                </div>

                                <div class="col">
                                    <label for="" class="form-label required">Enter sponsor phone</label>
                                    <input type="text" class="form-control" value="{{ $sponsor->phone }}" name="phone" placeholder="phone">
                                </div>

                                <div class="col">
                                    <label for="" class="form-label">Enter sponsor address</label>
                                    <input type="text" class="form-control" value="{{ $sponsor->address }}" name="address" placeholder="address">
                                </div>
                            </div>

                            <br>

                            <div class="row">
                                <div class="col">
                                    <button type="submit" class="btn btn-success">SUBMIT</button>
                                </div>
                            </div>
                        </form>

                        <br>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
