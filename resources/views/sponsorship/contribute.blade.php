@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header custom-card">
                        <div class="custom-card__left">
                            <b>MAKE CONTRIBUTION</b>
                        </div>

                        <div class="custom-card__right">
                            <a href="/children" class="btn btn-success">BACK</a>
                        </div>
                    </div>

                    <div class="card-body">

                        <div class="card-body child-details">

                            <div class="child-details__left">

                                <div class="custom-card">
                                    <div class="custom-card__left">
                                        <b>CHILD</b>
                                    </div>

                                    <div class="custom-card__right">

                                    </div>
                                </div>

                                <br>

                                <table class="table table-striped">
                                    <tbody>
                                    <tr>
                                        <th>Name</th>
                                        <td>{{ $child->first_name }} {{ $child->middle_name }} {{ $child->last_name }}</td>
                                    </tr>

                                    <tr>
                                        <th>Date of Birth</th>
                                        <td>{{ \Carbon\Carbon::parse($child->date_of_birth)->toFormattedDateString() }}</td>
                                    </tr>

                                    <tr>
                                        <th>Age</th>
                                        <td>{{ \Carbon\Carbon::now()->diffInYears(\Carbon\Carbon::parse($child->date_of_birth)) }} years</td>
                                    </tr>

                                    <tr>
                                        <th>Contribution</th>
                                        <td>{{ $child->amount ? $child->amount : null }} {{ $child->frequency ? $child->frequency->name : null }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="child-details__right">

                                <h5><b>Enter sponsorship amount and submit</b></h5>

                                <form method="POST" action="{{ url('/sponsorship/'.$child->id.'/contribute/post') }}">

                                    @csrf

                                    <input type="text" class="form-control" ng-model="amount" name="amount" placeholder="sponsorship amount">

                                    <br>

                                    <button class="btn btn-success" ng-click="assignSponsor()">SUBMIT</button>
                                </form>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            $(document).ready(function () {

                $('#example1').datepicker({
                    autoclose: true,
                    format: "yyyy-mm-dd"
                });

            });
        </script>
    </div>

@endsection
