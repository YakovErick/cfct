@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header custom-card">
                        <div class="custom-card__left">
                            <b>NEW SPONSORSHIP</b>
                        </div>

                        <div class="custom-card__right">
                            <a href="/children" class="btn btn-success">BACK</a>
                        </div>
                    </div>

                    <div class="card-body" ng-app="sponsorshipApp" ng-controller="SponsorshipController" ng-init="fetchChild('{!! $child_id !!}')">

                        <div class="card-body child-details">

                            <div class="child-details__left">

                                <div class="custom-card">
                                    <div class="custom-card__left">
                                        <b>CHILD</b>
                                    </div>

                                    <div class="custom-card__right">

                                    </div>
                                </div>

                                <br>

                                <table class="table table-striped">
                                    <tbody>
                                    <tr>
                                        <th>Name</th>
                                        <td>@{{ child.name }}</td>
                                    </tr>

                                    <tr>
                                        <th>Date of Birth</th>
                                        <td>@{{ child.dob }}</td>
                                    </tr>

                                    <tr>
                                        <th>Age</th>
                                        <td>@{{ child.age }} years</td>
                                    </tr>

                                    <tr>
                                        <th>Contribution</th>
                                        <td>@{{ child.contribution }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="child-details__right">

                                <h5><b>Search sponsor and assign to the child</b></h5>

                                <select name="sponsor_id" ng-model="sponsorship.sponsor_id"  class="form-control">

                                    <option
                                        ng-repeat="sponsor in sponsors"
                                        value='@{{ sponsor.id }}'>
                                        @{{ sponsor.first_name }}
                                        @{{ sponsor.middle_name }}
                                        @{{ sponsor.last_name }}
                                        -
                                        @{{ sponsor.email }}
                                        -
                                        @{{ sponsor.phone }}
                                    </option>

                                </select>

                                <br>

                                <button class="btn btn-success" ng-click="assignSponsor()">SUBMIT</button>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            $(document).ready(function () {

                $('#example1').datepicker({
                    autoclose: true,
                    format: "yyyy-mm-dd"
                });

            });
        </script>
    </div>

@endsection
