<div class="row">
    <div class="col">
        <label for="" class="form-label required">Enter child first name</label>
        <input type="text" class="form-control" ng-model="child.first_name" placeholder="first name">
    </div>

    <div class="col">
        <label for="" class="form-label">Enter child middle name</label>
        <input type="text" class="form-control" ng-model="child.middle_name" placeholder="middle name">
    </div>

    <div class="col">
        <label for="" class="form-label required">Enter child last name</label>
        <input type="text" class="form-control" ng-model="child.last_name" placeholder="last name">
    </div>
</div>

<br>

<div class="row">
    <div class="col">
        <label for="" class="form-label required">Enter date of birth</label>
        <input  class="form-control" type="text" placeholder="click to show datepicker"  id="example1" ng-model="child.date_of_birth">
    </div>

    <div class="col">
        <label for="" class="form-label required">Enter amount of sponsorship</label>
        <input type="text" class="form-control" ng-model="child.amount" placeholder="sponsorship amount">
    </div>

    <div class="col">
        <label for="" class="form-label required">Enter frequency of contibution</label>
        <select name="frequency_id" ng-model="child.frequency_id"  class="form-control">

            @foreach($frequencies as $frequency)
                <option value="{{ $frequency->id }}">{{ $frequency->name }}</option>
            @endforeach

        </select>
    </div>
</div>