@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header custom-card">
                        <div class="custom-card__left">
                            <b>CHILD INFORMATION</b>
                        </div>

                        <div class="custom-card__right">
                            <a href="/children" class="btn btn-success">BACK</a>
                        </div>
                    </div>

                    <div class="card-body child-details" ng-app="childApp" ng-controller="ChildrenController">

                        <div class="child-details__left">

                            <div class="custom-card">
                                <div class="custom-card__left">
                                    <b>CHILD</b>
                                </div>

                                <div class="custom-card__right">

                                    <a href="/children/{{ $child->id }}/edit" class="btn btn-success btn-sm">EDIT</a>

                                </div>
                            </div>

                            <br>

                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>Name</th>
                                        <td>{{ $child->fullName }}</td>
                                    </tr>

                                    <tr>
                                        <th>Date of Birth</th>
                                        <td>{{ \Carbon\Carbon::parse($child->date_of_birth)->toFormattedDateString() }}</td>
                                    </tr>

                                    <tr>
                                        <th>Age</th>
                                        <td>{{ Carbon\Carbon::now()->diffInYears(\Carbon\Carbon::parse($child->date_of_birth)) }} Years</td>
                                    </tr>

                                    <tr>
                                        <th>Contribution Amount</th>
                                        <td>KSH. {{ $child->amount }}</td>
                                    </tr>

                                    <tr>
                                        <th>Contribution Amount</th>
                                        <td>KSH. {{ $child->frequency->name }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="child-details__right">

                            <h5>PARENTS/GUARDIAN</h5>

                            <br>

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>NAME</th>
                                    <th>PHONE</th>
                                    <th>EMAIL</th>
                                    <th>ADDRESS</th>
                                </tr>
                                </thead>

                                <tbody>

                                @foreach($child->relationshipPersons as $parent)
                                    <tr>
                                        <td>{{ $parent->first_name . ' ' .$parent->middle_name . ' ' . $parent->last_name  }}</td>
                                        <td>{{ $parent->phone }}</td>
                                        <td>{{ $parent->email }}</td>
                                        <td>{{ $parent->address }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>


                    @if($child->sponsorships()->first())

                    <hr>

                    <div class="card-body child-details">

                        <div class="child-details__left">

                            <div class="custom-card">
                                <div class="custom-card__left">
                                    <b>SPONSORSHIP</b>
                                </div>

                                <div class="custom-card__right">

                                </div>
                            </div>

                            <br>

                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>Sponsor</th>
                                        <td>{{ $child->sponsorships()->first()->sponsor->fullName }}</td>
                                    </tr>

                                    <tr>
                                        <th>Email</th>
                                        <td>{{ $child->sponsorships()->first()->sponsor->email }}</td>
                                    </tr>

                                    <tr>
                                        <th>Phone</th>
                                        <td>{{ $child->sponsorships()->first()->sponsor->phone }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="child-details__right">

                            <div class="custom-card">
                                <div class="custom-card__left">
                                    <b>SPONSORSHIP</b>
                                </div>

                                <div class="custom-card__right">
                                    <a href="/sponsorship/{{ $child->id }}/contribute" class="btn btn-outline-success btn-sm">CONTRIBUTE</a>
                                </div>
                            </div>

                            <br>

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Amount</th>
                                </tr>
                                </thead>

                                <tbody>

                                @foreach($child->sponsorships()->orderBy('date_started', 'desc')->first()->contributions as $contribution)
                                    <tr>
                                        <td>{{ \Carbon\Carbon::parse($contribution->date)->toFormattedDateString() }}</td>
                                        <td>{{ $child->amount }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection