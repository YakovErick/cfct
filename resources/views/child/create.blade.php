@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header custom-card">
                        <div class="custom-card__left">
                            <b>NEW CHILD FORM</b>
                        </div>

                        <div class="custom-card__right">
                            <a href="/children" class="btn btn-success">BACK</a>
                        </div>
                    </div>

                    <div class="card-body" ng-app="childApp" ng-controller="ChildrenController">

                        <form>

                            <h6><b>CHILD DETAILS</b></h6>
                            <hr>

                            @include('child.form')

                            <br>
                            <br>

                            <h6><b>RELATIONSHIP PERSION</b></h6>
                            <hr>

                            @include('guardian.form')

                            <br>
                            <hr>

                            <div class="form-row">
                                <div class="col">
                                    <button type="button" class="btn btn-success" ng-click="submitChild()"  ng-if="guardian_added">SUBMIT</button>
                                </div>
                            </div>


                        </form>

                        <br>
                    </div>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            $(document).ready(function () {

                $('#example1').datepicker({
                    autoclose: true,
                    format: "yyyy-mm-dd"
                });

            });
        </script>
    </div>

@endsection
