@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header custom-card">
                        <div class="custom-card__left">
                            <b>CHILDREN MANAGEMENT</b>
                        </div>

                        <div class="custom-card__right">
                            <a href="/children/new" class="btn btn-success">NEW CHILD</a>
                        </div>
                    </div>

                    <div class="card-body" ng-app="childApp" ng-controller="ChildrenController">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>NAME</th>
                                <th>DATE OF BIRTH</th>
                                <th>AGE</th>
                                <th>PHONE</th>
                                <th>CONTRIBUTION</th>
                                <th>SPONSOR</th>
                                <th>DATE</th>
                                <th>ACTION</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr ng-repeat="child in children">
                                <td>@{{ child.name }}</td>
                                <td>@{{ child.dob }}</td>
                                <td>@{{ child.age }} Years</td>
                                <td>@{{ child.phone }}</td>
                                <td>KSH. @{{ child.contribution }}</td>
                                <td>
                                    <span ng-if="child.has_sponsorship">@{{ child.sponsor }}</span>
                                    <span ng-if="!child.has_sponsorship"><button ng-click="assignSponsor(child.id)" class="btn btn-primary btn-sm">ASSIGN</button></span>
                                </td>
                                <td>@{{ child.doa }}</td>
                                <td>
                                    <button ng-click="viewChild(child.id)" class="btn btn-info btn-sm">VIEW</button>
                                    <button ng-click="editChild(child.id)" class="btn btn-primary btn-sm">EDIT</button>
                                    <button ng-click="deleteChild(child.id)" class="btn btn-danger btn-sm">DELETE</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
