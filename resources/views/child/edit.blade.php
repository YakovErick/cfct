@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header custom-card">
                        <div class="custom-card__left">
                            <b>EDICT CHILD</b>
                        </div>

                        <div class="custom-card__right">
                            <a href="/children/{{ $child['id'] }}" class="btn btn-success">BACK</a>
                            <a href="/children" class="btn btn-info">ALL CHILDREN</a>
                        </div>
                    </div>

                    <div class="card-body">

                        <form method="POST" action="{{ route('edit_child', [ 'id' => $child->id ]) }}">

                            @csrf

                            <div class="row">
                                <div class="col">
                                    <label for="" class="form-label required">Enter child first name</label>
                                    <input type="text" class="form-control" value="{{ $child->first_name }}" name="first_name" placeholder="first name">
                                </div>

                                <div class="col">
                                    <label for="" class="form-label">Enter child middle name</label>
                                    <input type="text" class="form-control" value="{{ $child->middle_name }}" name="middle_name" placeholder="middle name">
                                </div>

                                <div class="col">
                                    <label for="" class="form-label required">Enter child last name</label>
                                    <input type="text" class="form-control" value="{{ $child->last_name }}" name="last_name" placeholder="last name">
                                </div>
                            </div>

                            <br>

                            <div class="row">
                                <div class="col">
                                    <label for="" class="form-label required">Enter date of birth</label>
                                    <input  class="form-control" type="text" placeholder="click to show datepicker" name="date_of_birth"  id="example1" value="{{ $child->date_of_birth }}">
                                </div>

                                <div class="col">
                                    <label for="" class="form-label required">Enter amount of sponsorship</label>
                                    <input type="text" class="form-control" value="{{ $child->amount }}" name="amount" placeholder="sponsorship amount">
                                </div>

                                <div class="col">
                                    <label for="" class="form-label required">Enter frequency of contibution</label>
                                    <select name="frequency_id" value="{{ $child->frequency_id }}" class="form-control">

                                        @foreach($frequencies as $frequency)
                                            <option value="{{ $frequency->id }}">{{ $frequency->name }}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <br>

                            <div class="form-row">
                                <div class="col">
                                    <button type="submit" class="btn btn-success">SUBMIT</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {

            $('#example1').datepicker({
                autoclose: true,
                format: "yyyy-mm-dd"
            });

        });
    </script>
@endsection