let SponsorshipService = angular.module('SponsorshipService', []);

SponsorshipService.config(function ($httpProvider) {

    let token = document.head.querySelector('meta[name="csrf-token"]');

    $httpProvider.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

    $httpProvider.defaults.headers.common['Authorization'] = 'Bearer ' + token.content;

    $httpProvider.defaults.headers.get = { "X-Requested-With": "XMLHttpRequest", "__RequestVerificationToken": $('[name=__RequestVerificationToken]').val() };
});

SponsorshipService.factory('SponsorshipData', ['$http', function ($http) {

        let SponsorshipData = {};

        SponsorshipData.fetchSponsorships = function () {
            return $http.get('/api/sponsorships');
        };

        SponsorshipData.addSponsorship = function (sponsorship) {
            return $http.post('/api/sponsorships', sponsorship);
        };

        SponsorshipData.deleteSponsorships = function (id) {
            return $http.delete('/api/sponsors/'+id);
        };

        SponsorshipData.getChild = function (id) {
            return $http.get('/api/children/'+id);
        };

        SponsorshipData.fetchSponsors = function () {
            return $http.get('/api/sponsors/all');
        };

        return SponsorshipData;
    }]);