let ChildrenService = angular.module('ChildrenService', []);

ChildrenService.config(function ($httpProvider) {

    let token = document.head.querySelector('meta[name="csrf-token"]');

    $httpProvider.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

    $httpProvider.defaults.headers.common['Authorization'] = 'Bearer ' + token.content;

    $httpProvider.defaults.headers.get = { "X-Requested-With": "XMLHttpRequest", "__RequestVerificationToken": $('[name=__RequestVerificationToken]').val() };
});

ChildrenService.factory('ChildData', ['$http', function ($http) {

    let ChildData = {};

    ChildData.fetchChildren = function () {
        return $http.get('/api/children');
    };

    ChildData.addChild = function (child) {
        return $http.post('/api/children', child);
    };

    ChildData.deleteChild = function (id) {
        return $http.delete('/api/children/'+id);
    };

    return ChildData;
}]);