let SponsorsService = angular.module('SponsorsService', []);

SponsorsService.config(function ($httpProvider) {

    let token = document.head.querySelector('meta[name="csrf-token"]');

    $httpProvider.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

    $httpProvider.defaults.headers.common['Authorization'] = 'Bearer ' + token.content;

    $httpProvider.defaults.headers.get = { "X-Requested-With": "XMLHttpRequest", "__RequestVerificationToken": $('[name=__RequestVerificationToken]').val() };
});

SponsorsService.factory('SponsorData', ['$http', function ($http) {

    let SponsorData = {};

    SponsorData.fetchSponsors = function () {
        return $http.get('/api/sponsors');
    };

    SponsorData.addSponsor = function (sponsor) {
        return $http.post('/api/sponsors', sponsor);
    };
    
    SponsorData.deleteSponsor = function (id) {
        return $http.delete('/api/sponsors/'+id);
    };

    return SponsorData;
}]);