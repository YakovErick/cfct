let authApp = angular.module("authApp", ['appService']);

authApp.config(function ($httpProvider) {

    let token = document.head.querySelector('meta[name="csrf-token"]');

    $httpProvider.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

    $httpProvider.defaults.headers.common['Authorization'] = 'Bearer ' + token.content;

    $httpProvider.defaults.headers.post = { "X-Requested-With": "XMLHttpRequest", "__RequestVerificationToken": $('[name=__RequestVerificationToken]').val() };
});
