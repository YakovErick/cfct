let sponsorshipApp = angular.module("sponsorshipApp", ['SponsorshipService']);

sponsorshipApp.controller('SponsorshipController', function($scope, SponsorshipData) {


    $scope.child = {};

    $scope.sponsorship = {};

    $scope.sponsors = [];

    fetchSponsors();

    function fetchSponsors() {

        SponsorshipData.fetchSponsors()
            .then( ({ data }) => {
                $scope.sponsors = data;
            }, () => {
                alert('Failed to fetch sponsor!');
            });
    }

    $scope.fetchChild = function (id) {

        $scope.sponsorship.child_id = id;

        SponsorshipData.getChild(id)
            .then( ({ data }) => {
                $scope.child = data.child;
            }, () => {
                alert('Failed to fetch child')
            })
    };

    $scope.assignSponsor = function () {

        SponsorshipData.addSponsorship($scope.sponsorship)
            .then( () => {

                location.replace('/children/'+$scope.sponsorship.child_id);
            }, () => {
                alert('Failed to create sponsorship!');
            });
    };
});