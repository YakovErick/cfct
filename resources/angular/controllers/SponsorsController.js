let sponsorApp = angular.module("sponsorApp", ['SponsorsService']);

sponsorApp.controller('SponsorsController', function($scope, SponsorData) {

    $scope.sponsor = {};

    $scope.sponsors = [];

    fetchSponsors();

    function fetchSponsors() {

        SponsorData.fetchSponsors()
            .then( ({ data }) => {
                $scope.sponsors = data.data;
            }, () => {
                alert('Failed to fetch sponsor!');
            });
    }

    $scope.submitSponsor = function () {

        SponsorData.addSponsor($scope.sponsor)
            .then( ({ data }) => {

                if(data.status == 406) {
                    alert("Kindly fill the missing fields and proceed")
                }
                else {
                    location.replace('/sponsors');
                    alert("Created successfully")
                }
            }, () => {
                alert('Failed to submit!');
            })
    };

    $scope.viewSponsor = function (id) {
        location.replace('/sponsors/'+id);
    };

    $scope.editSponsor = function (id) {
        location.replace('/sponsors/'+id+'/edit');
    };

    $scope.deleteSponsor = function (id) {

        let confi = confirm("Are you sure to delete a sponsor!");

        if(confi === true) {
            SponsorData.deleteSponsor(id)
                .then( ({ data }) => {

                    fetchSponsors();

                    if(data.status == 406) {
                        alert(data.message);
                    }

                }, () => {
                    fetchSponsors();
                });
        }
    }
});