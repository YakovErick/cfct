let childApp = angular.module("childApp", ['ChildrenService']);

childApp.controller('ChildrenController', function($scope, ChildData) {

    $scope.relationship_person = {
        relationship_id: 1
    };

    $scope.relationship_persons = [];

    $scope.child = {};

    $scope.children = [];

    $scope.guardian_added = false;

    fetchChildren();

    function fetchChildren() {
        ChildData.fetchChildren()
            .then( ({ data }) => {
                $scope.children = data.data;
            }, () => {
                alert('Failed to fetch children!');
            });
    }

    $scope.addGuardian = function () {

        $scope.relationship_persons.push($scope.relationship_person);

        $scope.guardian_added = true;

        $scope.relationship_person = {};
    };

    $scope.submitChild = () => {

        ChildData.addChild({
            child: $scope.child,
            relationship_persons: $scope.relationship_persons
        })
            .then( ({ data }) => {

                if(data.status == 406) {
                    alert("Kindly fill the missing fields and proceed")
                }
                else {
                    location.replace('/children');
                    alert("Created successfully")
                }

            }, () => {
                alert('Failed to submit!');
            })
    };

    $scope.viewChild = function (id) {
        location.replace('/children/'+id);
    };

    $scope.editChild = function (id) {
        location.replace('/children/'+id+'/edit');
    };

    $scope.deleteChild = function (id) {

        let confi = confirm("Are you sure to delete a child!");

        if(confi === true) {
            ChildData.deleteChild(id)
                .then( ({ data }) => {
                    fetchChildren();

                    if(data.status == 406) {
                        alert(data.message);
                    }
                }, () => {
                    fetchChildren();
                });
        }
    };

    $scope.assignSponsor = function (id) {
        location.replace('/sponsorship/'+id+'/create');
    }
});