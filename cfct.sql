-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 09, 2019 at 04:17 AM
-- Server version: 5.7.25-0ubuntu0.18.04.2
-- PHP Version: 7.3.4-1+ubuntu18.04.1+deb.sury.org+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cfct`
--

-- --------------------------------------------------------

--
-- Table structure for table `children`
--

CREATE TABLE `children` (
  `id` int(11) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `middle_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `frequency_id` int(11) DEFAULT NULL,
  `amount` decimal(20,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `children`
--

INSERT INTO `children` (`id`, `first_name`, `middle_name`, `last_name`, `date_of_birth`, `frequency_id`, `amount`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 'James', 'Maina', 'Kalhu', '2016-05-10 00:00:00', 3, '200.00', '2019-07-08 12:31:10', '2019-07-08 13:58:28', NULL),
(7, 'Banie', 'Jama', 'Laura', '2009-07-25 00:00:00', 1, '4500.00', '2019-07-08 13:51:50', '2019-07-09 01:43:09', NULL),
(8, 'Joash', 'Juma', 'Baraka', '2016-05-13 00:00:00', 3, '4000.00', '2019-07-09 03:54:45', '2019-07-09 03:54:45', NULL),
(9, 'Mary', 'Kilobi', 'Susan', '2018-07-12 00:00:00', 3, '34000.00', '2019-07-09 04:10:50', '2019-07-09 04:10:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_frequencies`
--

CREATE TABLE `payment_frequencies` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `slug` varchar(45) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `deleted_at` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_frequencies`
--

INSERT INTO `payment_frequencies` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Daily', 'daily', NULL, NULL, NULL),
(2, 'Weekly', 'weekly', NULL, NULL, NULL),
(3, 'Monthly', 'monthly', NULL, NULL, NULL),
(4, 'Quartely', 'quartely', NULL, NULL, NULL),
(5, 'Half Year', 'half-year', NULL, NULL, NULL),
(6, 'Annually', 'annually', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `relationships`
--

CREATE TABLE `relationships` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `slug` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `relationships`
--

INSERT INTO `relationships` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Father', 'father', '2019-07-07 00:00:00', '2019-07-07 00:00:00', NULL),
(2, 'Mother', 'mother', '2019-07-07 00:00:00', '2019-07-07 00:00:00', NULL),
(3, 'Brother', 'brother', '2019-07-07 00:00:00', '2019-07-07 00:00:00', NULL),
(4, 'Sister', 'sister', '2019-07-07 00:00:00', '2019-07-07 00:00:00', NULL),
(5, 'Aunt', 'aunt', '2019-07-07 00:00:00', '2019-07-07 00:00:00', NULL),
(6, 'Uncle', 'uncle', '2019-07-07 00:00:00', '2019-07-07 00:00:00', NULL),
(7, 'Guardian', 'giardian', '2019-07-07 00:00:00', '2019-07-07 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `relationship_persons`
--

CREATE TABLE `relationship_persons` (
  `id` int(11) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `middle_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `child_id` int(11) NOT NULL,
  `relationship_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `relationship_persons`
--

INSERT INTO `relationship_persons` (`id`, `first_name`, `middle_name`, `last_name`, `phone`, `email`, `address`, `child_id`, `relationship_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 'Kiamba', 'Karume', 'Maina', '0789093409', 'kkarume@gmail.com', 'Jamii Office', 6, 1, '2019-07-08 12:31:10', '2019-07-08 12:31:10', NULL),
(7, 'Florence', 'Kawi', 'Jockovic', '0709349012', 'jflo@gmail.com', 'Mimi street', 7, 1, '2019-07-08 13:51:50', '2019-07-08 13:51:50', NULL),
(8, 'James', 'Kawi', 'Mutu', '0708340923', 'mutu@gmail.com', 'Lane 18', 8, 1, '2019-07-09 03:54:45', '2019-07-09 03:54:45', NULL),
(9, 'Joan', 'Kilobi', 'Akwara', NULL, NULL, NULL, 9, 1, '2019-07-09 04:10:50', '2019-07-09 04:10:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sponsors`
--

CREATE TABLE `sponsors` (
  `id` int(11) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `middle_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sponsors`
--

INSERT INTO `sponsors` (`id`, `first_name`, `middle_name`, `last_name`, `phone`, `email`, `address`, `created_at`, `updated_at`, `deleted_at`) VALUES
(11, 'Lawrence', 'John', 'Shoomaker', '0709349023', 'shoomaker@gmail.com', 'Lane 34', '2019-07-08 12:59:47', '2019-07-08 12:59:47', NULL),
(13, 'James', 'Maina', 'Kuria', '07093490230', 'kuria@gmail.com', 'Lukenya 1', '2019-07-09 03:50:48', '2019-07-09 03:50:48', NULL),
(14, 'Joanne', NULL, 'Shang', '0709349012', 'shang@gmail.com', NULL, '2019-07-09 04:15:05', '2019-07-09 04:15:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sponsorships`
--

CREATE TABLE `sponsorships` (
  `id` int(11) NOT NULL,
  `child_id` int(11) NOT NULL,
  `sponsor_id` int(11) NOT NULL,
  `date_started` varchar(45) DEFAULT NULL,
  `date_ended` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `deleted_at` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sponsorships`
--

INSERT INTO `sponsorships` (`id`, `child_id`, `sponsor_id`, `date_started`, `date_ended`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 7, 11, '2019-07-08 18:32:11', NULL, NULL, '2019-07-08 18:32:11', '2019-07-08 18:32:11', NULL),
(2, 6, 11, '2019-07-08 18:57:52', NULL, NULL, '2019-07-08 18:57:52', '2019-07-08 18:57:52', NULL),
(3, 6, 11, '2019-07-08 18:57:59', NULL, NULL, '2019-07-08 18:57:59', '2019-07-08 18:57:59', NULL),
(4, 6, 11, '2019-07-08 18:58:22', NULL, NULL, '2019-07-08 18:58:22', '2019-07-08 18:58:22', NULL),
(5, 8, 13, '2019-07-09 03:54:57', NULL, NULL, '2019-07-09 03:54:57', '2019-07-09 03:54:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sponsorship_contibutions`
--

CREATE TABLE `sponsorship_contibutions` (
  `id` int(11) NOT NULL,
  `sponsorship_id` int(11) NOT NULL,
  `amount` decimal(20,2) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sponsorship_contibutions`
--

INSERT INTO `sponsorship_contibutions` (`id`, `sponsorship_id`, `amount`, `date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, '200.00', '2019-07-08 18:58:22', '2019-07-08 18:58:22', '2019-07-08 18:58:22', NULL),
(2, 1, '200.00', '2019-07-08 00:00:00', NULL, NULL, NULL),
(3, 4, '200.00', '2019-07-09 03:35:51', '2019-07-09 03:35:51', '2019-07-09 03:35:51', NULL),
(4, 4, '200.00', '2019-07-09 03:37:32', '2019-07-09 03:37:32', '2019-07-09 03:37:32', NULL),
(5, 4, '200.00', '2019-07-09 03:37:42', '2019-07-09 03:37:42', '2019-07-09 03:37:42', NULL),
(6, 5, '4000.00', '2019-07-09 03:54:57', '2019-07-09 03:54:57', '2019-07-09 03:54:57', NULL),
(7, 5, '4000.00', '2019-07-09 03:55:11', '2019-07-09 03:55:11', '2019-07-09 03:55:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `api_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', NULL, '$2y$10$dQnJeKAnXzNRoNQFsQ1fiujx7hFP2m4q3ShGvxv2mK7BY3Aw8W8ee', NULL, 'CZxYUcWw4w1tfcK7iqM1CWgoXPENQ3WcdkN0ulP4', '2019-07-06 07:16:05', '2019-07-09 02:47:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `children`
--
ALTER TABLE `children`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_children_payment_frequencies1_idx` (`frequency_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_frequencies`
--
ALTER TABLE `payment_frequencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `relationships`
--
ALTER TABLE `relationships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `relationship_persons`
--
ALTER TABLE `relationship_persons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_relationship_persons_children_idx` (`child_id`),
  ADD KEY `fk_relationship_persons_relationship1_idx` (`relationship_id`);

--
-- Indexes for table `sponsors`
--
ALTER TABLE `sponsors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsorships`
--
ALTER TABLE `sponsorships`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sponsorships_children1_idx` (`child_id`),
  ADD KEY `fk_sponsorships_sponsors1_idx` (`sponsor_id`);

--
-- Indexes for table `sponsorship_contibutions`
--
ALTER TABLE `sponsorship_contibutions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sponsorship_contibutions_sponsorships1_idx` (`sponsorship_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `children`
--
ALTER TABLE `children`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payment_frequencies`
--
ALTER TABLE `payment_frequencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `relationships`
--
ALTER TABLE `relationships`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `relationship_persons`
--
ALTER TABLE `relationship_persons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `sponsors`
--
ALTER TABLE `sponsors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `sponsorships`
--
ALTER TABLE `sponsorships`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sponsorship_contibutions`
--
ALTER TABLE `sponsorship_contibutions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `children`
--
ALTER TABLE `children`
  ADD CONSTRAINT `fk_children_payment_frequencies1` FOREIGN KEY (`frequency_id`) REFERENCES `payment_frequencies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `relationship_persons`
--
ALTER TABLE `relationship_persons`
  ADD CONSTRAINT `fk_relationship_persons_children` FOREIGN KEY (`child_id`) REFERENCES `children` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_relationship_persons_relationship1` FOREIGN KEY (`relationship_id`) REFERENCES `relationships` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sponsorships`
--
ALTER TABLE `sponsorships`
  ADD CONSTRAINT `fk_sponsorships_children1` FOREIGN KEY (`child_id`) REFERENCES `children` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sponsorships_sponsors1` FOREIGN KEY (`sponsor_id`) REFERENCES `sponsors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sponsorship_contibutions`
--
ALTER TABLE `sponsorship_contibutions`
  ADD CONSTRAINT `fk_sponsorship_contibutions_sponsorships1` FOREIGN KEY (`sponsorship_id`) REFERENCES `sponsorships` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
