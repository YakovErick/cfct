<?php


namespace App\FormValidations;


use Illuminate\Support\Facades\Validator;

trait ChildForm
{
    public function validateChild($data)
    {
        return Validator::make( $data, [
            'first_name' => 'required',
            'last_name' => 'required',
            'date_of_birth' => 'required',
            'amount' => 'required',
            'frequency_id' => 'required'
        ]);
    }

    public function validateRelationshipPerson($data)
    {
        return Validator::make( $data, [
            'first_name' => 'required',
            'last_name' => 'required',
            'date_of_birth' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'relationship_id' => 'required'
        ]);
    }
}