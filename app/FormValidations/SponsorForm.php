<?php


namespace App\FormValidations;


use Illuminate\Support\Facades\Validator;

trait SponsorForm
{
    public function validate($data)
    {
        return Validator::make( $data, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'email' => 'required',
        ]);
    }
}