<?php


namespace App\Resources\Transfomers;


use App\Models\Child;
use Carbon\Carbon;

class ChildTransformer
{
    public function transform(Child $child)
    {
        $guardian = $child->relationshipPersons()->first();

        $sponsorship = $child->sponsorships()->orderBy('date_started', 'desc')->first();

        $frequency = $child->frequency;

        return [
            'id' => $child->id,
            'name' => $child->fullName,
            'first_name' => $child->first_name,
            'middle_name' => $child->middle_name,
            'last_name' => $child->last_name,
            'dob' => Carbon::parse($child->date_of_birth)->toFormattedDateString(),
            'doa' => Carbon::parse($child->created_at)->toFormattedDateString(),
            'age' => Carbon::parse($child->date_of_birth)->diffInYears(Carbon::now()),
            'parent' => $guardian ? $guardian->fullName : null,
            'has_parent' => $guardian ? true : false,
            'sponsor' => $sponsorship ? $sponsorship->sponsor->fullName : null,
            'has_sponsorship' => $sponsorship ? true : false,
            'phone' => $guardian ? $guardian->phone : null,
            'parents' => $child->relationshipPersons,
            'contribution' => $frequency ? $child->amount . ' ' . $frequency->slug : null,
        ];
    }
}