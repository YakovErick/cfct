<?php


namespace App\Resources\Transfomers;


use App\Models\Child;
use App\Models\Sponsor;
use App\Models\Sponsorship;
use Carbon\Carbon;

class SponsorshipTransformer
{
    public function transform(Sponsorship $sponsor)
    {
        return [
            'id' => $sponsor->id,
            'name' => $sponsor->fullName,
            'email' => $sponsor->email,
            'phone' => $sponsor->phone,
            'address' => $sponsor->address
        ];
    }
}