<?php


namespace App\Resources\Transfomers;


use App\Models\Child;
use App\Models\Sponsor;
use Carbon\Carbon;

class SponsorsTransformer
{
    public function transform(Sponsor $sponsor)
    {
        return [
            'id' => $sponsor->id,
            'name' => $sponsor->fullName,
            'email' => $sponsor->email,
            'phone' => $sponsor->phone,
            'address' => $sponsor->address
        ];
    }
}