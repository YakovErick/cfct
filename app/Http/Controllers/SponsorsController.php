<?php


namespace App\Http\Controllers;


use App\Models\Sponsor;
use App\Repositories\SponsorRepository;
use Illuminate\Support\Facades\Redirect;

class SponsorsController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new SponsorRepository();
    }

    public function index()
    {
        return view('sponsor.index');
    }

    public function create()
    {
        return view('sponsor.create');
    }

    public function show($id)
    {
        $response = $this->repo->show($id);

        return view('sponsor.show', [
            'sponsor' => $response['sponsor']
        ]);
    }

    public function edit($id)
    {
        $sponsor = Sponsor::find($id);

        return view('sponsor.edit', [
            'sponsor' => $sponsor
        ]);
    }

    public function update($id)
    {
        $data = request()->except('_token');

        $this->repo->update($data, $id);

        return Redirect::back();
    }
}