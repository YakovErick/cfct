<?php


namespace App\Http\Controllers\Apis;


use App\Http\Controllers\Controller;
use App\Repositories\SponsorshipRepository;
use Illuminate\Http\Request;

class SponsorshipController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new SponsorshipRepository();
    }

    public function index()
    {
        return response()->json($this->repo->all());
    }

    public function store(Request $request)
    {
        $response = $this->repo->save($request->all());

        return response()->json($response);
    }

}