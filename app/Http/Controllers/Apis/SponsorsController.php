<?php


namespace App\Http\Controllers\Apis;


use App\Http\Controllers\Controller;
use App\Models\Sponsor;
use App\Repositories\SponsorRepository;
use Illuminate\Http\Request;

class SponsorsController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new SponsorRepository();
    }

    public function index()
    {
        return response()->json($this->repo->all());
    }

    public function all()
    {
        return response()->json(Sponsor::all());
    }

    public function store(Request $request)
    {
        $response = $this->repo->save($request->all());

        return response()->json($response);
    }

    public function destroy($id)
    {
        $response = $this->repo->delete($id);

        return response()->json($response);
    }
}