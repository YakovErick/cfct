<?php


namespace App\Http\Controllers\Apis;


use App\Http\Controllers\Controller;
use App\Repositories\ChildRepository;
use Illuminate\Http\Request;

class ChildrenController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new ChildRepository();
    }

    public function index()
    {
        return response()->json($this->repo->all());
    }

    public function store(Request $request)
    {
        $response = $this->repo->save($request->all());

        return response()->json($response);
    }

    public function show($id)
    {
        $result = $this->repo->show($id);

        return response()->json($result);
    }

    public function destroy($id)
    {
        $response = $this->repo->delete($id);

        return response()->json($response);
    }
}