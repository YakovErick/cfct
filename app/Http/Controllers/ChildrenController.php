<?php


namespace App\Http\Controllers;


use App\Models\Child;
use App\Models\PaymentFrequency;
use App\Models\Relationship;
use App\Repositories\ChildRepository;
use Illuminate\Support\Facades\Redirect;

class ChildrenController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new ChildRepository();
    }

    public function index()
    {
        return view('child.index');
    }

    public function create()
    {
        $relationships = Relationship::all();

        $frequencies = PaymentFrequency::all();

        return view('child.create',[
            'relationships' => $relationships,
            'frequencies' => $frequencies
        ]);
    }

    public function show($id)
    {
        $child = Child::find($id);

        return view('child.show', [
            'child' => $child
        ]);
    }

    public function edit($id)
    {
        $child = Child::find($id);

        $frequencies = PaymentFrequency::all();

        return view('child.edit', [
            'child' => $child,
            'frequencies' => $frequencies
        ]);
    }

    public function update($id)
    {
        $data = request()->except('_token');

        $this->repo->update($data, $id);

        return Redirect::back();
    }
}