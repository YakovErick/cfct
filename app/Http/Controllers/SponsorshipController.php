<?php


namespace App\Http\Controllers;


use App\Models\Child;
use App\Repositories\ContributionRepository;

class SponsorshipController extends Controller
{
    public function create($child_id)
    {
        return view('sponsorship.create', [
            'child_id' => $child_id
        ]);
    }

    public function contribute($child_id)
    {
        $child = Child::find($child_id);

        return view('sponsorship.contribute', [
            'child' => $child
        ]);
    }

    public function postContribution($id)
    {
        $child = Child::find($id);

        $input = request()->all();

        (new ContributionRepository())->create($child, $input);

        return view('child.show', [
            'child' => $child
        ]);
    }
}