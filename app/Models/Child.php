<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Child extends Model
{
    protected $table = 'children';

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'date_of_birth',
        'amount',
        'frequency_id'
    ];

    protected $guarded = [ 'id' ];

    public function getFullNameAttribute()
    {
        $first = $this->first_name;

        $middle = $this->middle_name ? ' ' . $this->middle_name . ' ' : ' ';

        $last = $this->last_name;

        return $first . $middle . $last;
    }

    public function relationshipPersons()
    {
        return $this->hasMany(RelationshipPerson::class, 'child_id');
    }

    public function sponsorships()
    {
        return $this->hasMany(Sponsorship::class, 'child_id');
    }

    public function frequency()
    {
        return $this->belongsTo(PaymentFrequency::class, 'frequency_id');
    }
}