<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class SponsorshipContribution extends Model
{
    protected $table = 'sponsorship_contibutions';

    protected $fillable = [
        'sponsorship_id',
        'date',
        'amount'
    ];

    protected $guarded = [ 'id' ];

    public function sponsorship()
    {
        return $this->belongsTo(Sponsorship::class, 'sponsorship_id');
    }
}