<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Sponsorship extends Model
{
    protected $table = 'sponsorships';

    protected $fillable = [
        'child_id',
        'sponsor_id',
        'date_started',
        'date_ended',
        'status'
    ];

    protected $guarded = [ 'id' ];

    public function child()
    {
        return $this->belongsTo(Child::class, 'child_id');
    }

    public function sponsor()
    {
        return $this->belongsTo(Sponsor::class, 'sponsor_id');
    }

    public function contributions()
    {
        return $this->hasMany(SponsorshipContribution::class, 'sponsorship_id');
    }
}