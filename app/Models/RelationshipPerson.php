<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class RelationshipPerson extends Model
{
    protected $table = 'relationship_persons';

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'phone',
        'email',
        'address',
        'child_id',
        'relationship_id'
    ];

    protected $guarded = [ 'id' ];

    public function getFullNameAttribute()
    {
        $first = $this->first_name;

        $middle = $this->middle_name ? ' ' . $this->middle_name . ' ' : ' ';

        $last = $this->last_name;

        return $first . $middle . $last;
    }

    public function relationship()
    {
        return $this->belongsTo(Relationship::class, 'relationship_id');
    }

    public function child()
    {
        return $this->belongsTo(Child::class, 'child_id');
    }
}