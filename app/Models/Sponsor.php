<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
    protected $table = 'sponsors';

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'phone',
        'email',
        'address',
    ];

    public function getFullNameAttribute()
    {
        $first = $this->first_name;

        $middle = $this->middle_name ? ' ' . $this->middle_name . ' ' : ' ';

        $last = $this->last_name;

        return $first . $middle . $last;
    }

    protected $guarded = [ 'id' ];

    public function sponsorships()
    {
        return $this->hasMany(Sponsorship::class, 'sponsor_id');
    }
}