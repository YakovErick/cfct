<?php


namespace App\Repositories;


use App\Models\Child;
use App\Models\SponsorshipContribution;
use Carbon\Carbon;

class ContributionRepository
{
    public function create(Child $child, $input)
    {
        $sponsorship = $child->sponsorships()->orderBy('date_started', 'desc')->first();

        $amount = isset($input['amount']) ? $input['amount'] : $child->amount;

        if(!$sponsorship) {
            return;
        }

        return SponsorshipContribution::create([
            'sponsorship_id' => $sponsorship->id,
            'amount' => $amount,
            'date' => Carbon::now()
        ]);
    }
}