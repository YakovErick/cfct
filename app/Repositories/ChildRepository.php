<?php


namespace App\Repositories;



use App\FormValidations\ChildForm;
use App\Models\Child;
use App\Resources\Transfomers\ChildTransformer;
use App\Traits\Paginate;
use Carbon\Carbon;
use Illuminate\Http\Response;

class ChildRepository
{
    use Paginate, ChildForm;

    public function all()
    {
        return $this->sortFilterPaginate(

            new Child(),

            [],

            function ($child) {
                return app(ChildTransformer::class)->transform($child);
            },

            function ($model) {
                return $model;
            }
        );
    }

    public function save($data)
    {
        $validator = $this->validateChild($data['child']);

        if($validator->fails()) {
            return [
                'message' => 'Complete the missing fields and proceed!',
                'errors' => $validator->errors(),
                'status' => Response::HTTP_NOT_ACCEPTABLE
            ];
        }

        $child_data  = $data['child'];

        $child_data['date_of_birth'] = isset($child_data['date_of_birth']) ? Carbon::parse($child_data['date_of_birth']) : null;

        $child = Child::create($child_data);

        $child->relationshipPersons()->createMany($data['relationship_persons']);

        return [
            'message' =>  'Child created successfully',
            'child' => $child,
            'status' => Response::HTTP_CREATED
        ];
    }

    public function show($id)
    {
        $child = Child::findOrFail($id);

        if(!$child) {
            return [
                'message' => 'Child with such id does not exist!',
                'status' => Response::HTTP_NOT_FOUND
            ];
        }

        return [
            'child' => (new ChildTransformer())->transform($child),
            'status' => Response::HTTP_ACCEPTED
        ];
    }

    public function update($data, $id)
    {
        $child = Child::findOrFail($id);

        $child->update($data);

        $child = (new ChildTransformer())->transform($child);

        return [
            'child' => $child,
            'status' => Response::HTTP_ACCEPTED
        ];
    }

    public function delete($id)
    {
        $child = Child::findOrFail($id);

        $sponsorship = $child->sponsorships()->first();

        if($sponsorship) {
            return [
                'message' => 'Cannot delete child because the child has continuing sponsorship!',
                'status' => Response::HTTP_NOT_ACCEPTABLE
            ];
        }

        $child->relationshipPersons->each( function ($relative) {
            $relative->delete();
        });

        $child->delete();

        return [
            'message' => 'Child deleted successfully!',
            'status' => Response::HTTP_ACCEPTED
        ];
    }
}