<?php


namespace App\Repositories;



use App\Models\Child;
use App\Models\Sponsorship;
use App\Models\SponsorshipContribution;
use Carbon\Carbon;
use Illuminate\Http\Response;

class SponsorshipRepository
{
    public function all()
    {
        return [];
    }

    public function save($data)
    {
        $data['date_started'] = Carbon::now();

        $child = Child::find($data['child_id']);

        $sponsorship = Sponsorship::create($data);

        SponsorshipContribution::create([
            'sponsorship_id' => $sponsorship->id,
            'date' => Carbon::now(),
            'amount' => $child->amount
        ]);

        return [
            'message' =>  'Sponsorship created successfully',
            'sponsorship' => $sponsorship,
            'status' => Response::HTTP_CREATED
        ];
    }

}