<?php


namespace App\Repositories;



use App\FormValidations\SponsorForm;
use App\Models\Sponsor;
use App\Resources\Transfomers\SponsorsTransformer;
use App\Traits\Paginate;
use Illuminate\Http\Response;

class SponsorRepository
{
    use Paginate, SponsorForm;

    public function all()
    {
        return $this->sortFilterPaginate(

            new Sponsor(),

            [],

            function ($sponsor) {
                return app(SponsorsTransformer::class)->transform($sponsor);
            },

            function ($model) {
                return $model;
            }
        );
    }

    public function save($data)
    {
        $validator = $this->validate($data);

        if($validator->fails()) {
            return [
                'message' => 'Complete the missing fields and proceed!',
                'errors' => $validator->errors(),
                'status' => Response::HTTP_NOT_ACCEPTABLE
            ];
        }

        $sponsor = Sponsor::create($data);

        return [
            'message' =>  'Sponsor created successfully',
            'sponsor' => $sponsor,
            'status' => Response::HTTP_CREATED
        ];
    }

    public function show($id)
    {
        $sponsor = Sponsor::findOrFail($id);

        if(!$sponsor) {
            return [
                'message' => 'Sponsor with such id does not exist!',
                'status' => Response::HTTP_NOT_FOUND
            ];
        }

        return [
            'sponsor' => (new SponsorsTransformer())->transform($sponsor),
            'status' => Response::HTTP_ACCEPTED
        ];
    }

    public function update($data, $id)
    {
        $sponsor = Sponsor::findOrFail($id);

        $sponsor->update($data);

        return [
            'sponsor' => $sponsor,
            'status' => Response::HTTP_ACCEPTED
        ];
    }

    public function delete($id)
    {
        $sponsor = Sponsor::findOrFail($id);

        $sponsorship = $sponsor->sponsorships()->first();

        if($sponsorship) {
            return [
                'message' => 'Cannot delete sponsor because sponsor has continuing sponsorship!',
                'status' => Response::HTTP_NOT_ACCEPTABLE
            ];
        }

        $sponsor->delete();

        return [
            'message' => 'Sponsor deleted successfully!',
            'status' => Response::HTTP_ACCEPTED
        ];
    }
}